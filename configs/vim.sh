#!/bin/bash

dir="$(dirname $0)"
srcdir="$dir/src";

#
# Add pathogen (plugin manager)
#
mkdir -p ~/.vim/autoload
cp "$srcdir/vim/autoload/pathogen.vim" ~/.vim/autoload/pathogen.vim

#
# .vimrc
#
cat "$dir/src/vimrc" >> ~/.vimrc

#
# Install plugins
#

# ale
git clone https://github.com/dense-analysis/ale ~/.vim/bundle/ale

# easy align
git clone https://github.com/junegunn/vim-easy-align.git ~/.vim/bundle/easy-align
