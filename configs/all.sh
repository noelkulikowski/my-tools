#!/bin/bash

dir="$(dirname $0)"
for s in "$dir"/*.sh; do
  if [ "$s" != "$dir/all.sh" ]; then
    echo $s
    $s
  fi
done
