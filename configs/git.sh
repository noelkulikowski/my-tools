#!/bin/bash

dir="$(dirname $0)"

cat "$dir/src/gitignore" >> ~/.gitignore
git config --global core.excludesFile ~/.gitignore
