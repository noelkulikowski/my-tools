#!/bin/bash

set -e

datafile=~/hours

function formatDistance {
  TZ=UTC0 formatDateAs '%H:%M' $1
}

function formatDateAs {
  date -j -f'%s' +"$1" $2
}

function printHistory {
  total=0
  while read start end rest; do
    if [ "$start" == '' ]; then
      if [ $total -ne 0 ]; then
        echo "  Total: $(formatDistance $total)"
      fi
      break
    fi

    this_date="$(formatDateAs '%D' $start)"
    if [ "$this_date" != "$last_date" ]; then
      if [ $total -ne 0 ]; then
        echo "  Total: $(formatDistance $total)"
      fi
      total=0
      echo $this_date
      last_date="$this_date"
    fi
    total=$(( $total + $end - $start))
    echo "  $(formatDateAs '%H:%M' $start) -- $(formatDistance $(( $end - $start )))"
  done < "$datafile"
}

function stopClock {
  intime=$1
  outtime="$( date +'%s' )"
  intimeiso="$( date -j -f '%s' -I minutes $intime )"
  outtimeiso="$( date -j -f '%s' -I minutes $outtime )"

  echo "$intime $outtime $intimeiso $outtimeiso" >> "$datafile"

  elapsed_seconds=$(( $outtime - $intime ))
  elapsed="$(formatDistance $elapsed_seconds)"
  echo "Clocked out.  $elapsed" 
}

while true; do
  inorout=$( [[ "$intime" == '' ]] && echo '"i" to clock in' || echo '"o" to clock out' )

  read -n1 -p "Hit $inorout; \"h\" to print history; \"q\" to quit: " reply
  echo ''

  case $reply in
    h)
      printHistory
      ;;
    i)
      if [ "$intime" == '' ]; then
        intime="$( date +'%s' )"
        echo "Clocked in at $(formatDateAs '%H:%M' $intime)."
      fi
      ;;
    o)
      if [ "$intime" != '' ]; then
        stopClock $intime
        intime=''
      fi
      ;;
    q)
      if [ "$intime" != '' ]; then
        stopClock $intime
        intime=''
      fi
      echo 'Bye'
      exit
      ;;
  esac
done
